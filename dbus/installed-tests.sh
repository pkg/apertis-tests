#!/bin/sh

# List of tests to skip (basename):
SKIP_TESTS="test-misc-internals test-service test-sleep-forever"

set -e

timeout="busybox timeout 300"
ret=0
i=0
tmpdir="$(mktemp -d)"

for dir in /usr/lib/dbus-1.0/installed-tests/dbus
do
    DBUS_TEST_DATA="${dir}/data"
    export DBUS_TEST_DATA
    DBUS_TEST_HOMEDIR="${tmpdir}/home"
    if ! test -d "$DBUS_TEST_HOMEDIR"
    then
        mkdir "$DBUS_TEST_HOMEDIR"
    fi
    export DBUS_TEST_HOMEDIR

    for t in "$dir"/test-*
    do
        # skip tests that have a .sh counterpart (execute only the
        # shell scripts in this case)
        test -f $t.sh && continue

        # skip tests marked to skip
        unset skip
        for AVOID_TEST in ${SKIP_TESTS}; do
          if [ "$(basename ${t})" = "${AVOID_TEST}" ]; then
            echo "TEST_RESULT:skip:$(basename "$t"):"
            skip=true
          fi
        done
        test -n "$skip" && continue

        i=$(( $i + 1 ))
        echo "# $i - $t ..."
        echo "x" > "$tmpdir/result"
        ( set +e; $timeout $t; echo "$?" > "$tmpdir/result" ) 2>&1 | sed 's/^/# /'
        # guard against a partial line
        echo '#'
        e="$(cat "$tmpdir/result")"
        case "$e" in
            (0)
                echo "TEST_RESULT:pass:$(basename "$t"):"
                ;;
            (77)
                echo "TEST_RESULT:skip:$(basename "$t"):"
                ;;
            (*)
                echo "TEST_RESULT:fail:$(basename "$t"):exit $e"
                ret=1
                ;;
        esac
    done
done

rm -r "$tmpdir"

if [ "$i" = 0 ]
then
    echo "# Did not execute any tests?!"
    exit 2
fi

exit $ret
