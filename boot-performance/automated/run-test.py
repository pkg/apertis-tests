#!/usr/bin/python3
# This script will execute all the systemd-analyze tests

from subprocess import Popen, PIPE

print("Running test systemd-analyze time ...")

args = ["systemd-analyze", "time"]

output, error = Popen(args, stdout=PIPE).communicate()

values = output.split()

for i in range(0, len(values)):
    if values[i].startswith("(") and values[i].endswith(")"):
        print(values[i] + "\t" + values[i - 1])

print("Running test systemd-analyze blame ...")

args = ["systemd-analyze", "blame"]

output, error = Popen(args, stdout=PIPE).communicate()

values = output.split()

for i in range(0, len(values), 2):
    print("(" + values[i + 1] + ")" + "\t" + values[i])
