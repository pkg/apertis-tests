# vim: set ts=8 tw=80 :

# Tools and arguments
CC		?= gcc
CFLAGS		?= -ggdb -O0 -pipe
PKG_CONFIG	?= pkg-config
APT_GET		?= sudo apt-get -y --force-yes
INSTALL		?= install

# Paths
PREFIX		?= /usr
LIBDIR		?= $(PREFIX)/lib/chaiwala-tests
DATADIR		?= $(PREFIX)/share/chaiwala-tests
TEST_NAME	?= $(shell echo $$(cd .. && echo $${PWD\#\#*/})/$${PWD\#\#*/})
TESTLIBDIR	 = $(LIBDIR)/$(TEST_NAME)
TESTDATADIR	 = $(DATADIR)/$(TEST_NAME)

# Common files provided by each test
SCRIPTS		?= config.sh run-test.sh
DOCS		?= README
CONFIG_FILE	:= inherit-config.sh

# These lowercase commented-out variables should be defined by the Makefile
#progs		:=

.MAKEDIRS:
ifneq ($(progs),)
	$(INSTALL) -m755 -d $(DESTDIR)$(TESTLIBDIR)
endif
ifneq ($(SCRIPTS)$(DOCS),)
	$(INSTALL) -m755 -d $(DESTDIR)$(TESTDATADIR)
endif

.INSTALL: .MAKEDIRS $(DOCS) $(SCRIPTS) $(CONFIG_FILE) $(progs)
ifneq ($(SCRIPTS)$(DOCS),)
	$(INSTALL) -m644 $(DOCS) $(DESTDIR)$(TESTDATADIR)
	$(INSTALL) $(SCRIPTS) $(DESTDIR)$(TESTDATADIR)
endif
ifneq ($(progs),)
	$(INSTALL) $(progs) $(DESTDIR)$(TESTLIBDIR);
endif
	# Tried to use m4 instead of sed, but sed is much simpler
	sed -e "s|^\(TESTDATADIR=\).*|\1\"$(TESTDATADIR)\"|" \
	    -e "s|^\(TESTLIBDIR=\).*|\1\"$(TESTLIBDIR)\"|" \
	    -e "s|^\(RESOURCE_DIR=\).*|\1\"$(DATADIR)/resources\"|" \
	    $(CONFIG_FILE) > $(DESTDIR)$(TESTDATADIR)/$(CONFIG_FILE)

.CLEAN:
	rm -f $(progs)
