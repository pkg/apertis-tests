#!/bin/sh

set -e
set -x

TMPFILE=`mktemp`

cleanup () {
	rv=$?

	# polkitd should be killed in the script body, so in that case the
	# following command will fail (hence the ||true). But if an error occurs
	# before the pkill is reached, then we still need to kill it here.
	sudo pkill -9 polkitd  >/dev/null 2>&1 || true
	rm -f "$TMPFILE"

	# Be nice and leave polkit running as normal.
	sudo systemctl restart polkit.service

	exit $rv
}

trap cleanup EXIT

# Restart polkitd to enable debug support (the standard systemd .service file
# starts it with --no-debug)
if [ -e /usr/lib/polkit-1/polkitd ]; then
	polkitd=/usr/lib/polkit-1/polkitd
else
	# polkitd is called with errexit set to "off", then
	# we have to fail before if the binary is not available.
	echo "The polkitd binary was not found!"
	exit 1
fi

( set +e; sudo ${polkitd} --replace 2>&1 ) | sed 's/^/# /' | tee $TMPFILE &

# Wait for it to start.
gdbus wait --address unix:path=/run/dbus/system_bus_socket \
        org.freedesktop.PolicyKit1

# For unknown reasons (race conditions?), pkaction randomly fails with:
# "Error enumerating actions: GDBus.Error:org.freedesktop.DBus.Error.NoReply:
#  Message recipient disconnected from message bus without replying"
# So, to mitigate this issue give it some more time to be ready,
# although this should be the aim of "gdbus wait" above.
sleep 5

# Query for actions, which forces polkit to parse all the policy files and
# output warnings if any are malformed.
#
# As a side effect, this gives a list of actions which _were_ successfully
# parsed in the test output.
#
pkaction

# Kill polkitd so the output actually gets written to TMPFILE. I have no idea
# why it doesn’t do this in the pipe.
sudo pkill -9 polkitd

# Any warnings in the polkitd output?
set +x

if grep WARNING "$TMPFILE"; then
	echo "TEST_RESULT:fail:polkit-parsing:"
	exit 1
else
	echo "TEST_RESULT:pass:polkit-parsing:"
	exit 0
fi
