#!/bin/sh

# This runs a CPU-intensive program while running Sysprof. If 

TEST_DIR=$(dirname $0)
TEST_PROG=${TEST_DIR}/fib.pl
# calculate the Fibonacci sequence to position 40. Runtime increases
# exponentially by this number
TEST_PROG_ARGS="36"
SYSPROF_OUT_FILE="sysprof-out.capture"

die() {
        echo $1
        exit 0
}

#
# This test has to be run as root for sysprof.
#
if [ "`whoami`" != "root" ]; then
        exec sudo -E $0
fi

#
# Clean-up from previous runs
#
rm -f $SYSPROF_OUT_FILE

#
# Main test
#

sysprof-cli $SYSPROF_OUT_FILE &
SYSPROF_PID=$!

$TEST_PROG $TEST_PROG_ARGS 2>&1
# terminate sysprof to have it write its output file
kill $SYSPROF_PID

kill_result=$?
if [ $kill_result -ne 0 ]; then
        echo "sdk-performance-tools-sysprof-smoke-test: FAILED"
        echo 2>&1 "ERROR: core test program finished before sysprof started."
        exit 0
fi

# ensure that sysprof has finished writing its file out
wait $SYSPROF_PID

STATUS_STR="FAILED"
# check that sysprof generated a binary capture file and that it is not empty
if [ $(stat --format=%s $SYSPROF_OUT_FILE) -gt 0 ]; then
        STATUS_STR="PASSED"
fi

echo "sdk-performance-tools-sysprof-smoke-test: $STATUS_STR"
