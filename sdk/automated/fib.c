/*
 * An intentionally-inefficient calculation of the summation of the Fibonacci
 * sequence to a given position.
 *
 * The purpose is to produce a process that is CPU-intensive and has a reliable
 * ordering of functions (by CPU time). This is so we can automatically check
 * whether sysprof is functioning as expected.
 */

#include <stdlib.h>
#include <stdio.h>

static int
fibonacci(int pos)
{
        switch(pos)
        {
                case 0:
                case 1:
                        return pos;
        }

        return fibonacci(pos-2) + fibonacci(pos-1);
}

int
main(int argc, char* argv[])
{
        int pos;

        if(argc < 2 || (pos = atoi(argv[1])) < 0)
        {
                printf("usage: %s POSITIVE_INTEGER\n", argv[0]);
                return 1;
        }

        printf("fibonacci(%d): %d\n", pos, fibonacci(pos));

        return 0;
}
