/*
 * Simple program which contains annotations and errors to be caught by splint
 */

#include <stdlib.h>
#include <stdio.h>

char
func_deref_null(/*@null@*/ char* null_arg)
{
  return *null_arg;
}

static int
func_unused_static()
{
  return 1;
}
