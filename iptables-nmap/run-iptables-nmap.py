#!/usr/bin/env python3
###################################################################################
# run-iptables-nmap.py
# Test that only valid services are found by nmap.
#
# Copyright (C) 2020 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import os
import sys
import xml.etree.ElementTree as ET

from subprocess import run


TMP_SCAN_FILE = '/tmp/nmap_scan.xml'

# These are the only valid services that should be found by nmap.
valid_services = [
    ('hotline', 'closed'),
    ('http', 'closed'),
    ('ssh', 'open')
]

def proc_xml():
    tree = ET.parse(TMP_SCAN_FILE)
    host = tree.find('host')
    ports = host.find('ports')

    only_valid_found = False
    invalid_found = False
    for port in ports.findall('port'):
        state = port.find('state').get('state')
        service = port.find('service').get('name')
        portid = port.get('portid')

        if (service, state) in valid_services:
            if not invalid_found:
                only_valid_found = True
        else:
            print(f"DEBUG:{service}/{portid}/{state}")
            if not invalid_found:
                only_valid_found = False
                invalid_found = True

    os.remove(TMP_SCAN_FILE)
    return only_valid_found

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Incorrect number of params")
        print(f"Usage: {sys.argv[0]} <ip_address>")
        exit(1)

    ipaddr = sys.argv[1]
    result_msg = "TEST_RESULT:only_valid_services:"

    cmd = run(f"nmap {ipaddr} -oX {TMP_SCAN_FILE}", shell=True, encoding="utf-8")
    if cmd.returncode != 0:
        print("DEBUG: nmap command fails")
        print(result_msg + "fail")
        exit(1)

    if proc_xml():
        print(result_msg + "pass")
        exit(0)
    else:
        print(result_msg + "fail")
        exit(1)
