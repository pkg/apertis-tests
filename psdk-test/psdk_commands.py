#
# This file contains the variables and commands for the persistent disk test.
#

# Variables for running the test.
# These can be easily changed to fit other type of configurations.

from subprocess import check_call, check_output, CalledProcessError
import gzip
import os
import paramiko
import shutil
import time
import urllib.request


class PsdkCommands(object):

    def __init__(self, old_sdk, new_sdk, user_test_file, vboxmanage_path,
                 local_port, username, password, retry, debug):
        self.vbox_manage = vboxmanage_path
        self.local_host = "127.0.0.1"
        self.local_port = int(local_port)
        self.old_sdk = old_sdk
        self.new_sdk = new_sdk
        self.user_test_file = user_test_file
        self.username = username
        self.password = password
        self.retry = int(retry)
        self.debug = debug
        self.ssh_client = None

    def _run(self, *commands, ssh=False, output=False):
        # Try to handle the case of connection reset errors on ssh
        # by calling several times the command if it fails.
        def _run_command(cmd, ssh, output, attempts=3):
            try:
                if output:
                    return check_output(cmd)
                else:
                    check_call(cmd)
            except CalledProcessError:
                if ssh and attempts > 0:
                    print("Trying again in 3 secs ...")
                    time.sleep(3)
                    _run_command(cmd, ssh, output, attempts - 1)
                else:
                    print("FAILED: %s" % cmd)
                    exit(1)
            except OSError:
                print("FAILED: Error running command: %s" % cmd)
                exit(1)

        for cmd in commands:
            if self.debug:
                print(cmd)
            if output:
                return _run_command(cmd, ssh, output)
            else:
                _run_command(cmd, ssh, output)

    def _ssh(self, cmd):
        if self.debug:
            print(cmd)
        _, _stdout, _stderr = self.ssh_client.exec_command(cmd)
        while not _stdout.channel.exit_status_ready() and \
              not _stdout.channel.recv_ready():
            time.sleep(1)
        _output = _stdout.read()
        _err = _stderr.read()
        exit_status = _stdout.channel.recv_exit_status()
        print(_output.decode())
        print(_err.decode())
        return exit_status, _output.decode(), _err.decode()

    def convert_ova_to_vdi(self,url, old_sdk, new_sdk):
        self._run(["tar", "-xvf", url])
        zipped = self._run(["find", ".", "-name", "*.gz"],output=True)
        zipped=zipped.decode('utf-8').strip()
        self._run(["gunzip", zipped])
        unzipped=zipped[:-3]
        self._run([self.vbox_manage, "clonehd", unzipped, "output.vdi", "--format=VDI"])
        self._run(["rm", unzipped])
        self._run([self.vbox_manage, "closemedium", "disk", "output.vdi"])
        self._run([self.vbox_manage, "closemedium", "disk", unzipped])
        shutil.copy("output.vdi", old_sdk)
        shutil.copy("output.vdi", new_sdk)

    def setup_disk_files(self, url, old_sdk_filename, new_sdk_filename):
        if url.startswith("http"):
            print("Dowloading %s ..." % url)
            try:
                response = urllib.request.urlopen(url)
                if (url.endswith("ova")):
                    with open('downloaded_file.ova', 'wb') as file:
                        content = response.read(1024*1024)
                        while(content):
                            file.write(content)
                            content = response.read(1024*1024)
                        self.convert_ova_to_vdi("downloaded_file.ova", old_sdk_filename, new_sdk_filename)
                        self._run(["rm", "downloaded_file.ova"])

                else:
                    gunzip_response = gzip.GzipFile(fileobj=response)
                    with open(old_sdk_filename, 'wb') as old_sdk_file, \
                        open(new_sdk_filename, 'wb') as new_sdk_file:
                        content = gunzip_response.read(1024*1024)
                        while (content):
                            old_sdk_file.write(content)
                            new_sdk_file.write(content)
                            content = gunzip_response.read(1024*1024)
            except urllib.error.HTTPError as e:
                print("FAILED: Error dowloading image file: ", e.code)
                exit(1)
            except urllib.error.URLError as e:
                print("FAILED: failed to reach server: ", e.reason)
                exit(1)
            print("Dowload completed")
        elif url.endswith("gz"):
            gunzip_response = gzip.GzipFile(filename=url)
            with open(old_sdk_filename, 'wb') as old_sdk_file, \
                 open(new_sdk_filename, 'wb') as new_sdk_file:
                content = gunzip_response.read(1024*1024)
                while (content):
                    old_sdk_file.write(content)
                    new_sdk_file.write(content)
                    content = gunzip_response.read(1024*1024)
        else:
            if(url.endswith("ova")):
                self.convert_ova_to_vdi(url, old_sdk_filename, new_sdk_filename)
            else:
                shutil.copy(url, old_sdk_filename)
                shutil.copy(url, new_sdk_filename)
        self._run([self.vbox_manage, "internalcommands", "sethduuid",
                  new_sdk_filename])

    def import_vm(self, vm, appliance):
        print("Importing VM (%s) for %s ..." % (appliance, vm))
        self._run(
            [ self.vbox_manage, "import", "--vsys", "0", "--vmname", vm,
              appliance ]
        )
        if self.local_port != 2222:
            self._run(
                [ self.vbox_manage, "modifyvm", vm, "--natpf1",
                  "delete", "apertis" ],
                [ self.vbox_manage, "modifyvm", vm, "--natpf1",
                  ("apertis,tcp,,%s,,22" % self.local_port) ]
            )

    def start_vm(self, vm, vmtype='headless'):
        retry = self.retry
        while retry > 0:
            self._run([self.vbox_manage, "startvm", vm, "--type", vmtype])
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.MissingHostKeyPolicy())
            timeout = 300
            timeout_start = time.time()
            while time.time() < timeout_start + timeout:
                time.sleep(5)
                try:
                    ssh_client.connect(self.local_host, port=self.local_port,
                                       username=self.username,
                                       password=self.password)
                except paramiko.ssh_exception.SSHException:
                    continue
                except paramiko.ssh_exception.NoValidConnectionsError:
                    break
                else:
                    print("Connected to %s" % vm)
                    self.ssh_client = ssh_client
                    return

            self.poweroff_vm(vm)
            retry -= 1

        if self.ssh_client is None:
            print("FAILED: Unable to connect to VM: %s" % vm)
            exit(1)

    def attach_vdi(self, vm, vdi_path):
        print("Attaching Disk %s ..." % vdi_path)
        self._run([ self.vbox_manage, "storageattach", vm, "--storagectl",
                    'SATA', "--port", "0", "--device", "0",
                    "--type", "hdd", "--medium", vdi_path ])

    def create_persistent_disk(self, pdisk_path):
        print("Creating persistent disk: %s" % pdisk_path)
        self._run([ self.vbox_manage, "createhd", "--size", "40000", "--variant",
                    "Standard", "--filename", pdisk_path ])

    def attach_persistent_disk(self, vm, pdisk_path):
        self._run([ self.vbox_manage, "storageattach", vm, "--storagectl",
                    'SATA', "--port", "1", "--device", "0",
                    "--type", "hdd", "--medium", pdisk_path ])

    def poweroff_vm(self, vm):
        if self.ssh_client:
            self.ssh_client.close()
            self.ssh_client = None
        self._run([ self.vbox_manage, "controlvm", vm, "acpipowerbutton" ])
        timeout = 60
        timeout_start = time.time()
        while self.is_vm_running(vm) and \
              time.time() < timeout_start + timeout:
            time.sleep(1)
        if self.is_vm_running(vm):
            print("Force poweroff %s" % vm)
            self._run([ self.vbox_manage, "controlvm", vm, "poweroff" ])
        if self.debug:
            print("Waiting 5 secs ...")
        time.sleep(5)

    def copy_file(self, cfile):
        filename = cfile.split(os.path.sep)[-1]
        ftp_client = self.ssh_client.open_sftp()
        ftp_client.put(cfile, "/home/user/" + filename)
        ftp_client.close()

    def move_proxy_file(self, proxy_test_file, apt_conf_dir):
        self._ssh("sudo mv " + "/home/user/" + proxy_test_file + " " +
                  apt_conf_dir)

    def copy_opt_file(self, cfile, opt_dir):
        filename = cfile.split(os.path.sep)[-1]
        tmp_filename = "tmp_" + filename
        ftp_client = self.ssh_client.open_sftp()
        ftp_client.put(cfile, "/home/user/" + tmp_filename)
        ftp_client.close()
        self._ssh("sudo mv " + "/home/user/" + tmp_filename + " " +
                  opt_dir + "/" + filename)

    def check_file(self, file1, file2):
        _status, _, _ = self._ssh("diff " + file1 + " " + file2)
        if _status == 0:
            return True
        return False

    def psdk_init(self):
        self._ssh("psdk --init")

    def psdk_configure(self):
        self._ssh("psdk --configure")

    def check_cmd(self, cmd, result):
        _, _output, _err = self._ssh(cmd)
        if result in _output:
            return True
        if result in _err:
            return True
        return False

    def psdk_add_file(self, file_path):
        self._ssh("psdk --etc " + file_path)

    def delete_vm(self, vm):
        print("Deleting VM %s ..." % vm)
        self._run([ self.vbox_manage, "unregistervm", vm, "--delete" ])

    def clean(self):
        print("Cleaning environment ...")
        if os.path.isfile(self.user_test_file):
            print("Removing", self.user_test_file)
            os.remove(self.user_test_file)
        if os.path.isfile(self.user_test_file + ".test"):
            print("Removing", self.user_test_file + ".test")
            os.remove(self.user_test_file + ".test")

        if self.is_vm_running(self.new_sdk):
            print("Stopping VM:", self.new_sdk)
            self.poweroff_vm(self.new_sdk)
        if self.is_vm_running(self.old_sdk):
            print("Stopping VM:", self.old_sdk)
            self.poweroff_vm(self.old_sdk)

        if self.vm_exists(self.old_sdk):
            self.delete_vm(self.old_sdk)
        if self.vm_exists(self.new_sdk):
            self.delete_vm(self.new_sdk)

    def is_vm_running(self, vm):
        vms = self._run([self.vbox_manage, "list", "runningvms"],
                        output=True).decode('utf-8')
        if self.debug:
            print(vms)
        for v in vms.split('\n'):
            lst = v.split()
            if len(lst) > 0:
                if lst[0] == ('"'+vm+'"'):
                    return True
        return False

    def vm_exists(self, vm):
        vms = self._run([self.vbox_manage, "list", "vms"],
                        output=True).decode('utf-8')
        if self.debug:
            print(vms)
        for v in vms.split('\n'):
            lst = v.split()
            if len(lst) > 0:
                if lst[0] == ('"'+vm+'"'):
                    return True
        return False
