#!/bin/bash

set -eEu

trap 'echo ; echo flatpak-demo-test: fail' ERR

. /etc/os-release # import $VERSION_ID
RELEASE=$VERSION_ID

flatpak --user remote-add --if-not-exists apertis https://images.apertis.org/flatpak/repo/apertis.flatpakrepo

flatpak install -y apertis org.apertis.headless.curl//${RELEASE}

flatpak run org.apertis.headless.curl https://www.apertis.org

echo
echo flatpak-demo-test: pass
