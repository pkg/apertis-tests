#!/usr/bin/env python3
###################################################################################
# apparmor-lockdown-nodeny.py
# Script to execute test case:
# https://qa.apertis.org/apparmor-session-lockdown-no-deny.html
#
# Copyright (C) 2020 Collabora Ltd
# Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import json
from subprocess import run


def show(msg):
    print("RESULT:%s" % msg)

def check_process(proc, mode):
    p = processes.get(proc)
    if p:
        if p[0]['status'] == mode:
            show("%s_%s_mode:pass" % (proc, mode))
        else:
            show("%s_%s_mode:fail" % (proc, mode))
    else:
        show("%s_process:fail" % proc)

if __name__ == '__main__':

    if run("pactl stat", shell=True).returncode != 0:
        show("pulseaudio_running:fail")
        exit(1)
    else:
        show("pulseaudio_running:pass")

    if run("sudo aa-status --enabled", shell=True).returncode != 0:
        show("apparmor_enabled:fail")
        exit(1)
    else:
        show("apparmor_enabled:pass")

    cmd = run("sudo aa-status --json", shell=True, capture_output=True)
    if cmd.returncode != 0:
        show("aa-status_json_process:fail")
        exit(1)
    processes = json.loads(cmd.stdout)['processes']

    check_process("/usr/sbin/connmand", "enforce")
    check_process("/usr/bin/pipewire", "enforce")
    check_process("/usr/bin/wireplumber", "enforce")
    check_process("/usr/sbin/ofonod", "enforce")

    c = run("sudo journalctl -b -t audit -o cat | aa_log_extract_tokens.sh DENIED",
            shell=True, encoding="utf-8", capture_output=True)
    if c.returncode != 0:
        show("audit_log_complaints:fail")
        print(str(c.stdout))
        exit(1)
    else:
        show("audit_log_complaints:pass")

    exit(0)
