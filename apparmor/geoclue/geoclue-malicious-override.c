/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things
 * to test whether apparmor works
 */

/* Easier for the build system */
#include "../common/function-system-malicious-override.c"

void
g_set_application_name (const char *application_name)
{
    void (*orig_f) (const char *);

    orig_f = dlsym (RTLD_NEXT, "g_set_application_name");

    do_malicious_stuff ();

    return orig_f (application_name);
}
