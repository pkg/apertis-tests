/* vim: set sts=4 sw=4 et :
 *
 * A small library that can be loaded using LD_PRELOAD to do malicious things
 * to test whether apparmor works
 */

#define _GNU_SOURCE
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#define CHAIWALA_USER "user"

static void
do_malicious_stuff (void)
{
  const char *filename = "/etc/passwd";

  if (fopen (filename, "r") == NULL)
    {
      fprintf (stderr, "Unable to be malicious: %s -- SUCCESS\n",
               strerror(errno));
    }
  else
    {
      fprintf (stderr, "Malicious code read contents of '%s' -- FAILURE\n",
               filename);
      /* Exit immediately if apparmor doesn't stop us. */
      exit (EXIT_FAILURE);
    }
}
