## Templates for Apertis tests

**Warning: The content of this repository are being moved to https://gitlab.apertis.org/tests/
to improve the testing worflow. New tests should be added to the new location.**

The templates contains the set of templates we use for lava testing for Apertis
and a set of default profiles. These are intended to be used with the lqa tool,
available from: http://cgit.collabora.com/git/singularity/tools/lqa.git/

For lqa usage see the readme in the lqa source tree, minimal examples
for Apertis below:

To submit the testsuite for headless images for minnowboard targets use:

    lqa submit -g ./apertis-tests/templates/profiles.yaml \
      --profile apertis-minimal-amd64-uefi \
      -t release:v2021dev1 \
      -t image_date:20191230.0 \
      -t image_name:apertis_v2021dev1-minimal-amd64-uefi_20191230.0

If you just want to issue a quicker boot test which does not execute the
testcases, specify the `minimal-tpl.yaml` job:

    lqa submit -g ./apertis-tests/templates/profiles.yaml \
      --profile apertis-minimal-amd64-uefi \
      -t release:v2021dev1 \
      -t image_date:20191230.0 \
      -t image_name:apertis_v2021dev1-minimal-amd64-uefi_20191230.0 \
      ./apertis-tests/templates/minimal-tpl.yaml

The image_date variable specifies the image data of the image to use, see
https://images.apertis.org/daily/ for the list of available images.
