#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

# Randomly select 42 as a random number
[ -z "$RANDOM" ] && RANDOM=42

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

# Initialization and setup shared between the test scripts
. "${TESTDIR}/init-setup.sh"
setup_success

###########
# Execute #
###########

start_rule() {
  TITLE="$1"

  BIND_IPSRC=$2
  BIND_TCPSPORT=$3

  ENFORCED_IPSRC=$4
  ENFORCED_IPDST=$5
  ENFORCED_TCPSPORT=$6
  ENFORCED_TCPDPORT=$7
  ENFORCED_BACKGROUND_RATE=$8
  ENFORCED_PRIORITY_RATE=$9

  EXPECTED_BACKGROUND_RATE=${10}
  EXPECTED_PRIORITY_RATE=${11}

  title "Test: $TITLE"

  ${TESTDIR}/client.py \
    --ipsrc "$ENFORCED_IPSRC" --ipdst "$ENFORCED_IPDST" \
    --tcpsport "$ENFORCED_TCPSPORT" --tcpdport "$ENFORCED_TCPDPORT" \
    --backgroundrate "$ENFORCED_BACKGROUND_RATE" \
    --priorityrate "$ENFORCED_PRIORITY_RATE" &
  CLIENT_PID=$!

  say "Please look at the download rate."
  /bin/sleep 0.5
  wget -T 5 -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST="${PROC_KILL_LIST} $WGET_PID"

  sleep $WGET_TEST_DURATION
  kill $WGET_PID || true
  /bin/sleep 0.2
  whine "Was the limit of $EXPECTED_BACKGROUND_RATE correctly enforced [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    kill $CLIENT_PID || true
    return 1
  fi

  say "Please look at the download rate."
  /bin/sleep 0.5
  say wget -T 5 --bind-address="$BIND_IPSRC" --bind-port="$BIND_TCPSPORT" -O /dev/null $URL
  wget -T 5 --bind-address="$BIND_IPSRC" --bind-port="$BIND_TCPSPORT" -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST="${PROC_KILL_LIST} $WGET_PID"

  sleep $WGET_TEST_DURATION
  kill $WGET_PID || true
  /bin/sleep 0.2
  whine "Was the limit of $EXPECTED_PRIORITY_RATE correctly enforced [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    kill $CLIENT_PID || true
    return 1
  fi

  kill $CLIENT_PID
}

check_decent_speed() {
  TITLE="$1"

  title "Test: $TITLE"

  say "Please look at the download rate."
  wget -T 5 -O /dev/null $URL &
  WGET_PID=$!
  PROC_KILL_LIST="${PROC_KILL_LIST} $WGET_PID"

  sleep $WGET_TEST_DURATION
  kill -9 $WGET_PID || true
  wait $WGET_PID || true
  /bin/sleep 0.2
  whine "Was it a decent speed (1MB/s or more) [yes/no]?"
  read v
  if [ "$v" != "yes" ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  return 0
}

run_test () {
    if "$2" "$1" $3
    then
        echo -n "$1: "
        echo_green PASS
    else
        echo -n "$1: "
        echo_red FAIL
    fi
    echo
}

check_tcmmd() {
  sudo $TCMMD -i $NET_INTERFACE >/dev/null 2>&1 &
  TCMMD_PID=$!
  ROOT_PROC_KILL_LIST="${ROOT_PROC_KILL_LIST} $TCMMD_PID"
  /bin/sleep 0.5
  if ! sudo kill -s 0 $TCMMD_PID > /dev/null 2>&1 ; then
    return 1
  fi
  /bin/sleep 0.5

  # there is no point to run the main tests if we don't have a good connectivity
  check_decent_speed "check decent speed before the main tests"
  if [ $? != 0 ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  say "Ok, let's start the main tests."

  TCPSPORT=$(( 12000 + $RANDOM % 999 ))

  run_test "TC rule checking the tuple (ipsrc, ipdst, tcpsport, tcpdport)" start_rule "$IP $TCPSPORT $IP $SERVER_IP $TCPSPORT 80 10000 80000 10KB/s 80KB/s"

  run_test "TC rule checking only tcpsport" start_rule "$IP $TCPSPORT 0.0.0.0 0 $TCPSPORT 0 10000 80000 10KB/s 80KB/s"

  run_test "TC rule with a wrong ipsrc" start_rule "$IP $TCPSPORT 8.8.8.8 $SERVER_IP $TCPSPORT 80 10000 80000 10KB/s 10KB/s"

  run_test "TC rule with a wrong ipdst" start_rule "$IP $TCPSPORT $IP 8.8.8.8 $TCPSPORT 80 10000 80000 10KB/s 10KB/s"

  run_test "TC rule with a wrong tcpsport" start_rule "$IP $TCPSPORT $IP $SERVER_IP $(( $TCPSPORT + 1 )) 80 10000 80000 10KB/s 10KB/s"

  run_test "TC rule with a wrong tcpdport" start_rule "$IP $TCPSPORT $IP $SERVER_IP $TCPSPORT 81 10000 80000 10KB/s 10KB/s"

  run_test "check if decent speed is restored after the main tests" check_decent_speed ""

  echo

  sudo kill $TCMMD_PID || true

  check_decent_speed "check if connectivity is not broken after killing tcmmd"
  if [ $? != 0 ] ; then
    sudo kill $TCMMD_PID || true
    return 1
  fi

  return 0
}

cleanup_and_maybe_fail () {
    s=$?
    _cleanup
    [ $s -eq 0 ] || test_failure
}
trap "cleanup_and_maybe_fail" EXIT

check_tcmmd

